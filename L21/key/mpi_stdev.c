#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <mpi.h>

typedef unsigned long long int uint64;

int main (int argc, char** argv) {

    MPI_Init (&argc, &argv);

    int rank, size;
    MPI_Comm_rank(MPI_COMM_WORLD,&rank);
    MPI_Comm_size(MPI_COMM_WORLD,&size);

    /* get N from the command line */
    if (argc < 2) {
        printf ("Command usage : %s %s\n",argv[0],"N");
        return 1;
    }
    uint64 N = atol(argv[1]);

    /* start the timer */
    double start_time, end_time;
    start_time = MPI_Wtime();

    /* each rank computes a partial sum */
    double sum = 0;
    for (uint64 i=1+rank;i<=N;i+=size) {
        sum += i;
    }

    /* rank 0 sums up partial sums from each rank */
    /* and rank 0 broadcasts the total sum to all other ranks */
    /* Only use MPI_IN_PLACE when using MPI_Allreduce not when using MPI_Reduce */
    MPI_Allreduce(MPI_IN_PLACE,&sum,1,MPI_DOUBLE,MPI_SUM,MPI_COMM_WORLD);
#if 0
    double rank_sum = sum;
    MPI_Allreduce(&rank_sum,&sum,1,MPI_DOUBLE,MPI_SUM,MPI_COMM_WORLD);
#endif
#if 0
    MPI_Reduce(&rank_sum,&sum,1,MPI_DOUBLE,MPI_SUM,0,MPI_COMM_WORLD);
#endif
#if 0
    if (rank == 0) {
        double rank_sum;
        MPI_Status status;
        for (int source=1;source<size;source++) {
            MPI_Recv (&rank_sum,1,MPI_DOUBLE,source,0,MPI_COMM_WORLD,&status);
            sum += rank_sum;
        }
    } else {
        int dest = 0;
        MPI_Send (&sum,1,MPI_DOUBLE,dest,0,MPI_COMM_WORLD);
    }
#endif

#if 0
    /* rank 0 broadcasts the total sum to all other ranks */
    MPI_Bcast(&sum,1,MPI_DOUBLE,0,MPI_COMM_WORLD);
#endif
#if 0
    if (rank == 0) {
        for (int dest=1;dest<size;dest++) {
            MPI_Send (&sum,1,MPI_DOUBLE,dest,0,MPI_COMM_WORLD);
        }
    } else {
        int source = 0;
        MPI_Status status;
        MPI_Recv (&sum,1,MPI_DOUBLE,source,0,MPI_COMM_WORLD,&status);
    } 
#endif

    /* every rank has the correct sum and can now compute the mean */
    double mean = sum/N;

    /* each rank computes a partial sum of difference squares */
    double sum_diff_sq = 0;
    for (uint64 i=1+rank;i<=N;i+=size) {
        sum_diff_sq += (i-mean)*(i-mean);
    }

    /* rank 0 sums up partial sums of difference squares from each rank */ 
    double rank_sum_diff_sq = sum_diff_sq;
    MPI_Reduce(&rank_sum_diff_sq,&sum_diff_sq,1,MPI_DOUBLE,MPI_SUM,0,MPI_COMM_WORLD);
#if 0
    if (rank == 0) {
        double rank_sum_diff_sq;
        MPI_Status status;
        for (int source=1;source<size;source++) {
            MPI_Recv (&rank_sum_diff_sq,1,MPI_DOUBLE,source,0,MPI_COMM_WORLD,&status);
            sum_diff_sq += rank_sum_diff_sq;
        }
    } else {
        int dest = 0;
        MPI_Send (&sum_diff_sq,1,MPI_DOUBLE,dest,0,MPI_COMM_WORLD);
    }
#endif

    /* only rank 0 has the correct sum of diff sqs */
    /* and can now compute the correct variance */
    /* (other ranks will not compute the correct variance) */
    double variance = sum_diff_sq/N;

    /* calculate the standard deviation */
    double stdev = sqrt(variance);

    /* stop the timer */
    end_time = MPI_Wtime();

#ifdef DEBUG
    printf ("rank %d elapsed time = %g\n",rank,end_time-start_time);
    printf ("rank %d standard deviation is %.4lf, sqrt((N^2-1)/12) is %.4lf\n",
            rank,stdev,sqrt((N*N-1)/12.0));
#endif

    /* only rank 0 has the correct standard deviation */
    if (rank == 0) {
#ifdef STUDY
        printf ("(%d,%.4f),",size,(end_time-start_time));
#else
        printf ("num ranks = %d, elapsed time = %g\n",size,end_time-start_time);
        printf ("standard deviation is %.4lf, sqrt((N^2-1)/12) is %.4lf\n",
                stdev,sqrt((N*N-1)/12.0));
#endif
    }

    MPI_Finalize();
}
