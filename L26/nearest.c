#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

typedef unsigned char byte;

/* calculates ||u-v||^2 */
/* the inputs u and v are byte arrays and the output is an integer */
int vec_dist_sq (byte* u, byte* v, int dim) {
    int dist_sq = 0;
    for (int i=0;i<dim;i++) {
        dist_sq += (u[i]-v[i])*(u[i]-v[i]);
    }
    return dist_sq;
}

/* GOAL: for each test vector find the nearest training vector */
/* INPUTS: train and test are pointers to byte matrices with dim columns */
/*  that store the training set and test set */
/* num_train is the number of training vectors and num_test is the number of test vectors */
/* OUTPUT: nearest is an array that contains the indices of the nearest training vectors */
/* Note: INT_MAX is the largest possible C integer value */
void nearest(byte* train, int num_train, byte* test, int num_test, int* nearest, int dim) {

    /*****************/
    /* Add Code Here */
    /*****************/

}
