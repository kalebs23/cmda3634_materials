#!/bin/bash
#SBATCH -A cmda3634_rjh
#SBATCH -p normal_q
#SBATCH -t 00:10:00
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=4
#SBATCH -o mpi_stdev.out

# Go to the directory where the job was submitted
cd $SLURM_SUBMIT_DIR

# Load the modules we need for MPI
module load matplotlib

# Build the executables
mpicc -o mpi_stdev mpi_stdev.c -lm

# run mpi standard deviation
# use mpiexec -n $SLURM_NTASKS to run on all allocated resources
#mpiexec -n 1 --map-by ppr:$SLURM_NTASKS_PER_NODE:node ./mpi_stdev $1
#mpiexec -n 2 --map-by ppr:$SLURM_NTASKS_PER_NODE:node ./mpi_stdev $1
mpiexec -n 4 --map-by ppr:$SLURM_NTASKS_PER_NODE:node ./mpi_stdev $1
