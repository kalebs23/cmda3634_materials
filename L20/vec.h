#ifndef VEC_H
#define VEC_H

/* calculates ||u-v||^2 */
double vec_dist_sq (double* u, double* v, int dim);

/* read a vector from a binary file of unsigned chars */
void vec_read_bin (double* data, int dim, char* filename, int header_size);

#endif 

