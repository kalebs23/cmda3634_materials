#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <mpi.h>

typedef unsigned long long int uint64;

int main (int argc, char** argv) {

    MPI_Init (&argc, &argv);

    int rank, size;
    MPI_Comm_rank(MPI_COMM_WORLD,&rank);
    MPI_Comm_size(MPI_COMM_WORLD,&size);

    /* get N from the command line */
    if (argc < 2) {
        printf ("Command usage : %s %s\n",argv[0],"N");
        return 1;
    }
    uint64 N = atol(argv[1]);

    /* start the timer */
    double start_time, end_time;
    start_time = MPI_Wtime();

    /* compute the mean */
    double sum = 0;
    for (uint64 i=1;i<=N;i++) {
        sum += i;
    }
    double mean = sum/N;

    /* compute the variance */
    double sum_diff_sq = 0;
    for (uint64 i=1;i<=N;i++) {
        sum_diff_sq += (i-mean)*(i-mean);
    }
    double variance = sum_diff_sq/N;

    /* compute the standard deviation */
    double stdev = sqrt(variance);

    /* stop the timer */
    end_time = MPI_Wtime();

    /* print the results */
    printf ("rank %d elapsed time = %g\n",rank,end_time-start_time);
    printf ("rank %d standard deviation is %.4lf, sqrt((N^2-1)/12) is %.4lf\n",
            rank,stdev,sqrt((N*N-1)/12.0));

    MPI_Finalize();
}
