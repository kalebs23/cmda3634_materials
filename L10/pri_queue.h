#ifndef PRI_QUEUE_H
#define PRI_QUEUE_H

typedef struct pri_queue_element_s {
    double priority;
    int data;
} pri_queue_element;

typedef struct pri_queue_s {
    pri_queue_element* elements;
    int max_size;
    int size;
} pri_queue;

void pri_queue_init(pri_queue* pq, int max_size);

int pri_queue_size(pri_queue* pq);

void pri_queue_insert(pri_queue* pq, pri_queue_element element);

pri_queue_element pri_queue_peek_top(pri_queue* pq);

void pri_queue_delete_top(pri_queue* pq);

void pri_queue_free(pri_queue* pq);

#endif
