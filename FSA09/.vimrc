" Set all encoding to utf-8
set enc=utf-8
set fenc=utf-8
set termencoding=utf-8

" Disable vi compatibility
set nocompatible

" Syntax highlighting + indentation
syntax enable
filetype plugin indent on

" No line wrapping
set nowrap

" Tab size is 4, expand tabs to spaces
set tabstop=4
set shiftwidth=4
set expandtab
set softtabstop=4

" Enable line numbers
set number

" Makes backspace behave like normal 
" (https://vi.stackexchange.com/questions/2162/
"  why-doesnt-the-backspace-key-work-in-insert-mode)
set backspace=indent,eol,start
