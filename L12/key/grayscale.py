import sys # for command line arguments
from PIL import Image, ImageOps # for image processing
import numpy as np # for matrix processing

# make sure command line arguments are provided
if (len(sys.argv) < 3):
    print ('command usage :',sys.argv[0],'color_image','gray_image')
    exit(1)

# open the color image and convert it to a 3d array    
color_image = Image.open(sys.argv[1])
A = np.array(color_image)
rows,cols,colors = A.shape
print ('A has',rows,'rows,',cols,'columns, and',colors,'color channels')
print ('datatype of A is',A.dtype)

# create a 2d array for the grayscale image
G = np.zeros((rows,cols),dtype='uint8')

# convert the color matrix to a grayscale matrix
for i in range(rows):
    for j in range(cols):
        red = A[i][j][0]
        green = A[i][j][1]
        blue = A[i][j][2]
        G[i][j] = round(0.299*red+0.587*green+0.114*blue)

# convert the 2d array of gray intensity values to an image        
gray_image = Image.fromarray(G)

# save the grayscale image
gray_image.save(sys.argv[2])
