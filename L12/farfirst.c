#include <float.h>
#include "vec.h"

/* calculate the arg max */
int calc_arg_max (double* data, int rows, int cols, int* centers, int m) {
    int arg_max;
    double cost_sq = 0;
    for (int i=0;i<rows;i++) {
        double min_dist_sq = DBL_MAX;
        for (int j=0;j<m;j++) {
            double dist_sq = vec_dist_sq(data+i*cols,data+centers[j]*cols,cols);
            if (dist_sq < min_dist_sq) {
                min_dist_sq = dist_sq;
            }
        }
        if (min_dist_sq > cost_sq) {
            cost_sq = min_dist_sq;
            arg_max = i;
        }
    }
    return arg_max;
}

/* calculate the cost squared */
double calc_cost_sq (double* data, int rows, int cols, int* centers, int k) {
    double cost_sq = 0;
    for (int i=0;i<rows;i++) {
        double min_dist_sq = DBL_MAX;
        for (int j=0;j<k;j++) {
            double dist_sq = vec_dist_sq(data+i*cols,data+centers[j]*cols,cols);
            if (dist_sq < min_dist_sq) {
                min_dist_sq = dist_sq;
            }
        }
        if (min_dist_sq > cost_sq) {
            cost_sq = min_dist_sq;
        }
    }
    return cost_sq;
}
