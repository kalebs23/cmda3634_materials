import time
import sys
import numpy as np

# build a table of distances squared 
def build_dist_sqs_table(pts,dist_sqs):
    n = len(pts)
    for i in range(n):
        for j in range(n):
            dist_sqs[i][j] = np.inner(pts[i]-pts[j],pts[i]-pts[j])

# compute the cost of using centers c1, c2, c3
def centers_cost_sq(dist_sqs,c1,c2,c3,min_cost_sq):
    cost_sq = 0
    n = len(dist_sqs)
    for i in range(n):
        dist_sq_1 = dist_sqs[i][c1]
        dist_sq_2 = dist_sqs[i][c2]
        dist_sq_3 = dist_sqs[i][c3]
        min_dist_sq = min(dist_sq_1,dist_sq_2,dist_sq_3)
        # check to see if we can abort early
        if (min_dist_sq > min_cost_sq):
            return min_dist_sq
        cost_sq = max(cost_sq,min_dist_sq)
    return cost_sq

# find the optimal cost by checking each pair of centers
def solve_3center(dist_sqs,opt_ctrs):
    min_cost_sq = float("inf")
    n = len(pts)
    tuples_checked = 0
    for i in range(0,n-2):
        for j in range(i+1,n-1):
            for k in range(j+1,n):
                cost_sq = centers_cost_sq(dist_sqs,i,j,k,min_cost_sq)
                tuples_checked += 1
                if (cost_sq < min_cost_sq):
                    min_cost_sq = cost_sq
                    opt_ctrs[0] = i
                    opt_ctrs[1] = j
                    opt_ctrs[2] = k
    return (np.sqrt(min_cost_sq),tuples_checked)

pts = np.loadtxt(sys.argv[1])
n = len(pts)
opt_ctrs = np.array([0,0,0])
dist_sqs = np.zeros((n,n))
build_dist_sqs_table(pts,dist_sqs)
start = time.process_time()
(min_cost,tuples_checked) = solve_3center(dist_sqs,opt_ctrs)
elapsed = time.process_time()-start
print ('number of points =',len(pts))
print ('3-tuples checked =',tuples_checked)
print ('elapsed time =',np.round(elapsed,5),'seconds')
print ('3-tuples checked per second =',int(tuples_checked/elapsed))
print ('minimum cost =',np.round(min_cost,5))
print ('optimal centers =',opt_ctrs)
