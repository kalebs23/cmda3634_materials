#include <stdio.h>
#include <stdlib.h>
#include <float.h>
#include <math.h>
#include <time.h>

#define MAX_POINTS 2000

/* read the file of points */
int read_file (float* points, char* filename) {

    int n = 0;
    float next[2];
    FILE* file_ptr;
    file_ptr = fopen(filename,"r");
    if (file_ptr == NULL) {
        printf ("error : could not open file %s for reading\n",filename);
        exit(1);
    }
    while (fscanf (file_ptr,"%f %f",next,next+1) == 2) {
        if (n < MAX_POINTS) {
            points[2*n] = next[0];
            points[2*n+1] = next[1];
            n += 1;
        } else {
            printf ("Too many points in file %s\n",filename);
            fclose (file_ptr);
            exit(1);
        }
    }
    fclose (file_ptr);
    return n;
}

/* calculate the distance squared between two points */
float calc_dist_sq (float* u, float* v) {
    float diff_x = u[0] - v[0];
    float diff_y = u[1] - v[1];
    return (diff_x*diff_x + diff_y*diff_y);
}

/* compute the cost squared for centers c1, c2, c3, c4 */
float center_cost_sq (int n, float* dist_sqs, int c1, int c2, int c3, int c4, float min_cost_sq) {
    float cost_sq = 0;
    for (int i=0;i<n;i++) {
        float dist_sq_1 = dist_sqs[i*n+c1];
        float dist_sq_2 = dist_sqs[i*n+c2];
        float dist_sq_3 = dist_sqs[i*n+c3];
        float dist_sq_4 = dist_sqs[i*n+c4];
        float min_dist_sq = dist_sq_1;
        if (dist_sq_2 < min_dist_sq) {
            min_dist_sq = dist_sq_2;
        }
        if (dist_sq_3 < min_dist_sq) {
            min_dist_sq = dist_sq_3;
        }
        if (dist_sq_4 < min_dist_sq) {
            min_dist_sq = dist_sq_4;
        }
        /* check to see if we can abort early */
        if (min_dist_sq > min_cost_sq) {
            return min_dist_sq;
        }
        if (min_dist_sq > cost_sq) {
            cost_sq = min_dist_sq;
        }
    }
    return cost_sq;
}

int main (int argc, char** argv) {

    /* the points array */
    float points[2*MAX_POINTS];

    /* read filename from command line */
    if (argc < 2) {
        printf ("Command usage : %s %s\n",argv[0],"filename");
        return 1;
    }

    /* read dataset */
    int n = read_file (points,argv[1]);

    /* calculate the distances squared table */
    float dist_sqs[n*n];
    for (int i=0;i<n;i++) {
        for (int j=0;j<n;j++) {
            dist_sqs[i*n+j] = calc_dist_sq (points+2*i,points+2*j);
        }
    }

    /* start the timer */
    clock_t tic,toc;
    tic = clock();

    /* solve the 4-center problem exactly */
    double min_cost_sq = DBL_MAX;
    int optimal_centers[4];
    int tuples_checked = 0;
    for (int i=0;i<n-3;i++) {
        for (int j=i+1;j<n-2;j++) {
            for (int k=j+1;k<n-1;k++) {
                for (int l=k+1;l<n;l++) {
                    tuples_checked += 1;
                    double cost_sq = center_cost_sq (n, dist_sqs, i, j, k, l, min_cost_sq);
                    if (cost_sq < min_cost_sq) {
                        min_cost_sq = cost_sq;
                        optimal_centers[0] = i;
                        optimal_centers[1] = j;
                        optimal_centers[2] = k;
                        optimal_centers[3] = l;
                    }
                }
            }
        }
    }

    /* stop the timer */
    toc = clock();

    /* print the number of points */
    printf ("number of points = %d\n",n);

    /* print out the number of 4-tuples checked */
    printf ("4-tuples checked = %d\n",tuples_checked);

    /* print the elapsed time */
    double elapsed = (double)(toc-tic)/CLOCKS_PER_SEC;
    printf ("elapsed time = %g seconds\n",elapsed);

    /* print the tuples checked per second */
    printf ("4-tuples checked per second = %.0lf\n",tuples_checked/elapsed);

    /* print the minimal cost for the 4-center problem */
    printf ("minimal cost = %g\n",sqrt(min_cost_sq));

    /* print an optimal solution to the 4 center problem */
    printf ("optimal centers : %d %d %d %d\n",
            optimal_centers[0],optimal_centers[1],optimal_centers[2],
            optimal_centers[3]);

    return 0;
}
