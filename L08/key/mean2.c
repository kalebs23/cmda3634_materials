#include <stdio.h>
#include "float2.h"

int main () {

    float2 sum = { 0, 0 };
    float2 point;
    int n = 0;
    while (scanf("%f %f",&(point.x),&(point.y)) == 2) {
        sum = float2_add (sum,point);
        n += 1;
    }
    if (n < 1) {
        printf ("Error : not enough points!\n");
        return 1;
    }
    float2 mean = float2_scalar_mult (sum,1.0/n);
    float2_print (mean,"mean");
}
