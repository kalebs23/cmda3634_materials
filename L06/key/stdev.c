#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define MAX_ARRAY_SIZE 1000

int main () {

    /* array for storing the numbers read from the file */
    double numbers[MAX_ARRAY_SIZE];

    /* read the data from the file */
    int n = 0;
    double number;
    while (scanf("%lf",&number) == 1) {
        if (n < MAX_ARRAY_SIZE) {
            numbers[n] = number;
            n += 1;
        } else {
            printf ("Too many numbers!\n");
            return 1;
        }
    }

    /* to compute sample variance we need at least 2 numbers */
    if (n<2) {
        printf ("Not enough numbers!\n");
        return 1;
    }

    /* calculate and print the sample mean */
    double sum = 0;
    for (int i=0;i<n;i++) {
        sum += numbers[i];
    }
    double sample_mean = sum/n;
    printf ("The sample mean is %.2lf\n",sample_mean);

    /* calculate and print the sample variance */
    double sum_diff_sq = 0;
    for (int i=0;i<n;i++) {
        sum_diff_sq += (numbers[i]-sample_mean)*(numbers[i]-sample_mean);
    }
    double sample_variance = sum_diff_sq/(n-1);
    printf ("The sample variance is %.2lf\n",sample_variance);

    /* calculate and print the standard deviation */
    double sample_stdev = sqrt(sample_variance);
    printf ("The standard deviation is %.2lf\n",sample_stdev);
}
