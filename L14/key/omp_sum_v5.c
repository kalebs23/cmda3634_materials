#include <stdio.h>
#include <stdlib.h>
#include <omp.h>

typedef unsigned long long uint64_t;

int main(int argc, char **argv) {

    /* get N and thread_count from command line */
    if (argc < 3) {
        printf ("Command usage : %s %s %s\n",argv[0],"N","thread_count");
        return 1;
    }
    uint64_t N = atol(argv[1]);
    int thread_count = atoi(argv[2]);
    omp_set_num_threads(thread_count);

    /* start the timer */
    double start_time, end_time;
    start_time = omp_get_wtime();

    /* calculate the sum */
    uint64_t sum = 0;

    // The read only shared variable N is fine, but one problem with the 
    // current the code is that there is a read/write race condition for sum.
    // 
    // Here is what we might imagine happens when two different threads 
    // access sum during the first iteration of the for loop:
    // Thread 1 reads the current value of sum : 0
    // Thread 1 adds i=1 to the current value of sum : 1
    // Thread 1 stores the new value of sum : 1 
    // Thread 2 reads the current value of sum : 1
    // Thread 2 adds i=1 to the current value of sum : 2
    // Thread 2 stores the new value of sum : 2
    // 
    // Since the threads operate concurrently,
    // what is actually happening could be more like:
    // Thread 1 reads the current value of sum : 0
    // Thread 2 reads the current value of sum : 0
    // Thread 1 adds i=1 to the current value of sum : 1
    // Thread 2 adds i=1 to the current value of sum : 1
    // Thread 1 stores the new value of sum : 1
    // Thread 2 stores the new value of sum : 1
    // In this case part of the sum is lost and we end up 
    // computing a value of the sum < thread_count*N*(N+1)/2
    // (this is exactly what happens when we run the code).
    //
    // There are a couple of ways to fix this issue.
    // One way is to have threads update sum inside of a critical region.  
    // By putting a block of code in a parallel region after a
    // "#pragma omp critical" that block of code must be executed by a
    // single thread at a time. 
    // Use a critical region to fix the read/write race condition for sum.

#pragma omp parallel default(none) shared(N,sum)
    {
        for (uint64_t i = 1; i <= N;i++) {
#pragma omp critical
            {
                sum += i;
            }
        }
    }

    /* stop the timer */
    end_time = omp_get_wtime();

    printf ("thread_count = %d, ",thread_count);
    printf ("elapsed time = %g\n",end_time-start_time);
    printf ("sum = %llu\n",sum);
    printf ("N*(N+1)/2 = %llu\n",(N/2)*(N+1));
}

