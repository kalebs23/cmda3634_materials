#include <stdio.h>
#include <stdlib.h>
#include <float.h>

int main () {
    double smallest = DBL_MAX;
    double number;
    int num_read = 0;
    while (scanf("%lf",&number) == 1) {
        if (number < smallest) {
            smallest = number;
        }
        num_read++;
    }
    if (num_read > 0) {
        printf ("The smallest number is %lf\n",smallest);
    }
}
