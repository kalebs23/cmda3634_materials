#include <stdio.h>
#include <stdlib.h>
#include <float.h>

int main (int argc, char** argv) {
    if (argc < 2) {
        printf ("Command usage : %s %s\n",argv[0],"center");
        return 1;
    }
    double center = atof(argv[1]);
    double number;
    double closest;
    double min_distance_sq = DBL_MAX;
    int num_read = 0;
    while (scanf("%lf",&number) == 1) {
        double distance_sq = (center-number)*(center-number);
        if (distance_sq < min_distance_sq) {
            closest = number;
            min_distance_sq = distance_sq;
        }
        num_read++;
    }
    if (num_read > 0) {
        printf ("The number closest to %lf is %lf\n",center,closest);
    }
    return 0;
}

