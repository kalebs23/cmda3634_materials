#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>

typedef unsigned long long uint64_t;

int main(int argc, char **argv) {

    MPI_Init (&argc, &argv);

    int rank, size;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    /* get N from command line */
    if (argc < 2) {
        printf ("Command usage : %s %s\n",argv[0],"N");
        return 1;
    }
    uint64_t N = atol(argv[1]);

    /* start the timer */
    double start_time, end_time;
    start_time = MPI_Wtime();

    /* calculate the sum */
    uint64_t sum = 0;
    for (uint64_t i = 1+rank; i <= N;i+=size) {
	sum += i;
    }

    /* stop the timer */
    end_time = MPI_Wtime();

    /* use collective communication to calculate the total sum */
    uint64_t total_sum = 0;
    // MPI_Reduce(&sum,&total_sum,1,MPI_UNSIGNED_LONG_LONG,MPI_SUM,0,MPI_COMM_WORLD);
    // MPI_Bcast(&total_sum,1,MPI_UNSIGNED_LONG_LONG,0,MPI_COMM_WORLD);
    MPI_Allreduce(&sum,&total_sum,1,MPI_UNSIGNED_LONG_LONG,MPI_SUM,MPI_COMM_WORLD);

    printf ("rank %d (of %d) sum = %llu, total sum = %llu, N*(N+1)/2 = %llu\n",
	    rank,size,sum,total_sum,(N/2)*(N+1));
    printf ("rank %d elapsed time = %.4lf seconds\n",rank,end_time-start_time);
    
    MPI_Finalize();

}

