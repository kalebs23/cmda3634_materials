#!/bin/bash
#SBATCH -A cmda3634_rjh
#SBATCH -p normal_q
#SBATCH -t 00:10:00
#SBATCH --nodes=2
#SBATCH --ntasks-per-node=2
#SBATCH -o mpi_sum_v5.out

# Go to the directory where the job was submitted
cd $SLURM_SUBMIT_DIR

# Load the modules we need for MPI
module load matplotlib

# Build the executable
mpicc -o mpi_sum_v5 mpi_sum_v5.c

# run mpi_sum_v5
# use mpiexec -n $SLURM_NTASKS to run on all allocated resources
mpiexec -n 4 --map-by ppr:$SLURM_NTASKS_PER_NODE:node ./mpi_sum_v5 $1
